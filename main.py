# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from datetime import datetime

import json
import os

import tweepy
from kafka import KafkaConsumer

consumer = KafkaConsumer(os.environ['topic'], bootstrap_servers=os.environ['boostrap_server'], group_id="twitter-consumer")

auth = tweepy.OAuthHandler(
    os.environ['consumer_key'],
    os.environ['consumer_secret_key'],
    os.environ['access_key'],
    os.environ['access_secret_key']
)

api = tweepy.API(auth)

try:
    api.verify_credentials()
    print("Connection")
except:
    print("Error")

oldStations: dict = {
}

for stations in consumer:
    stationsJson = json.loads(stations.value.decode())
    for stationJson in stationsJson:
        name = stationJson['name']
        if name in oldStations.keys():
            differance = oldStations[name] - stationJson['available_bike_stands']
            if differance != 0 and oldStations[name] != 0:
                if differance <= 0:
                    date = datetime.now()
                    api.update_status("Change available bike stands : " + str(name) + " : "
                                      + str(differance) + " at " + str(date.hour) + ":"
                                      + str(date.minute) + ":" + str(date.second))
                else:
                    date = datetime.now()
                    api.update_status("Change available bike stands : " + str(name) + " : +"
                                      + str(differance) + " at " + str(date.hour) + ":"
                                      + str(date.minute) + ":" + str(date.second))
        else:
            oldStations[name] = stationJson['available_bike_stands']

