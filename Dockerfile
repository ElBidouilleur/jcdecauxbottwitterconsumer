FROM python:latest

RUN mkdir /app

WORKDIR /app

COPY . .

RUN pip install --no-cache-dir tweepy
RUN pip install --no-cache-dir kafka-python

CMD ["python", "./main.py"]
